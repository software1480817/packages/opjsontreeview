unit uOpJSONTreeview;
// https://edn.embarcadero.com/print/40882
interface

uses
  packages.tools.VersionInformation,
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.ComCtrls,
  {$IF CompilerVersion >= 28.0}System.Json,{$ENDIF}
  DBXJSON;

type
  TOpJSONTreeview = class(TTreeView)

  private
    FJSONObject: TJSONObject;
    FVisibleChildrenCounts: Boolean;
    FVisibleByteSizes: Boolean;

    procedure SetJSONObject(const Value: TJSONObject);
    procedure SetVisibleChildrenCounts(const Value: Boolean);
    procedure SetVisibleByteSizes(const Value: Boolean);
    procedure ProcessElement(currNode: TTreeNode; arr: TJSONArray;
      aIndex: integer);
    procedure ProcessPair(currNode: TTreeNode; obj: TJSONObject;
      aIndex: integer);
    { Private-Deklarationen }
    function ParseTreeChild(currNode: TTreeNode; obj: TJsonObject) : boolean;
  protected
    { Protected-Deklarationen }
    procedure Notification(AComponent: TComponent;
      Operation: TOperation); override;
  public
    { Public-Deklarationen }
    bReadOnly : boolean;
    class function IsSimpleJsonValue(v: TJSONValue): boolean; inline;
    class function UnQuote(s: string): string; inline;
    class function RemoveQuotation(aValue: String): String; inline;
	class function IsArray(Name : string) : boolean; inline;
    constructor Create(AOwner: TComponent); override;
    procedure ClearAll;
    procedure LoadJson;
    procedure DumpJson(aJsonObject : TJsonObject);

    property JSONObject: TJSONObject read FJSONObject write SetJSONObject;
  published
    { Published-Deklarationen }
  end;

  // returning Version information from gitversion.inc
  function GetVersionInformation: TExtractFileVersionData;
  function GetVersionNumber: String;
  procedure Register;

implementation

function GetVersionInformation: TExtractFileVersionData;
begin
  Result := VersionInformation.ExtractInformations;
end;

function GetVersionNumber: string;
begin
  Result :=GetVersionInformation.FileVersionData.FileVersionNr;
end;

procedure Register;
begin
  RegisterComponents('opVcl', [TOpJSONTreeview]);
end;

{ TJSONTreeView }

procedure TOpJSONTreeview.ClearAll;
begin
  if Assigned(Items) then
    Items.Clear;
end;

constructor TOpJSONTreeview.Create(AOwner: TComponent);
begin
  inherited;
  bReadOnly := false;
  FVisibleChildrenCounts := false;
  FVisibleByteSizes := false;
end;

procedure TOpJSONTreeview.DumpJson(aJsonObject: TJsonObject);
var
   TreeView :  TOpJSONTreeview;
   Node     :  TTreeNode;
//   temp     : string;
begin
  TreeView  := self;
  if TreeView.Items.Count = 0 then
    Exit;
  Node := TreeView.Items[0].getFirstChild;
  if not Assigned(aJsonObject) then
    aJsonObject.Create;
  while Assigned(Node) do
  begin
     if Node.HasChildren then
       ParseTreeChild(Node, aJsonObject) ;
     Node := TreeView.Items[0].GetNextChild(Node);
  end;
end;

class function TOpJSONTreeview.IsSimpleJsonValue(v: TJSONValue): boolean;
begin
  Result := (v is TJSONNumber)
    or (v is TJSONString)
    or (v is TJSONTrue)
    or (v is TJSONFalse)
    or (v is TJSONNull);
end;

procedure TOpJSONTreeview.LoadJson;
var
  v: TJSONValue;
//  currNode: TTreeNode;
  i, aCount: integer;
//  s: string;
begin
  ClearAll;
  if Assigned(JSONObject) then // and JSONObject.IsActive then
  begin
    v := JSONObject;
    Items.Clear;

    if IsSimpleJsonValue(v) then
      Items.Add(nil, UnQuote(v.Value))     // ohne gesweifte klammern oder Hockommas
    else
      if v is TJSONObject then
      begin
        aCount := TJSONObject(v).Count;
        for i := 0 to aCount - 1 do
          ProcessPair(nil, TJSONObject(v), i);
      end

      else
      if v is TJSONArray then
      begin

        aCount := TJSONArray(v).Count;
        for i := 0 to aCount - 1 do
          //ProcessElement(currNode, TJSONArray(v), i)
          ProcessElement(nil, TJSONArray(v), i)
      end
  end;
end;

function TOpJSONTreeview.ParseTreeChild(currNode: TTreeNode;
  obj: TJsonObject): boolean;
var
    loJSA  :  TJsonArray;
    loJSP  :  TJSONPair;
    loJSO  :  TJsonObject;
    child  :  TTreeNode;
    temp   : string;
begin
   result := true;

   child := currNode.GetFirstChild;

   loJSA := TJsonArray.Create();
   loJSO := TJsonObject.Create();
   while Assigned(child) do
   begin
      if child.HasChildren then
      begin
        ParseTreeChild(child, loJSO);
        temp  :=  loJSO.Tostring;
      end else
      begin
        temp := child.Text;
        if pos('"',child.Text) > 0 then
          loJSA.Add(RemoveQuotation(child.Text))
        else if (child.Text = 'true') or (child.Text = 'false') then
          loJSA.Add(child.Text = 'true')
        else
          loJSA.Add(strtofloatdef(child.Text,0));
        temp := loJSA.toString
      end;
      child := currNode.GetNextChild(child);
   end;
   if currNode.GetFirstChild.HasChildren then
     loJSP := TJSONPair.Create(currNode.Text, loJSO)
   else begin
     temp := currNode.Text;
     if IsArray(temp) then
       temp := System.copy(temp,0,length(temp) - 2);
     if  (not IsArray(currNode.Text)) and (loJSA.Count = 1) then
       loJSP := TJSONPair.Create(temp, loJSA.Items[0] as TJSONvalue)
     else
       loJSP := TJSONPair.Create(temp, loJSA);
   end;
   obj.AddPair(loJSP);
end;

class function TOpJSONTreeview.IsArray(Name : string) : boolean;
begin
   Result := System.copy(Name, length(Name) - 1, 2) = '[]';
end;

class function TOpJSONTreeview.RemoveQuotation(aValue: String): String;
var
  trimmed : string;
begin
  trimmed := trim(aValue);
  if (pos('"',trimmed) = 1) and (copy(trimmed, length(trimmed),1) = '"') and (copy(trimmed, length(trimmed)-1,1) <> '\') then
  begin
    Result := UnQuote(trimmed);
  end else
    Result := aValue;
  Result := Result.Replace('\\','\');
end;

procedure TOpJSONTreeview.ProcessPair(currNode: TTreeNode; obj: TJSONObject; aIndex: integer);
  var
    p: TJSONPair;
    s: string;
    node: TTreeNode;
    i, aCount: integer;
begin
  p := obj.Pairs[aIndex];
  ReadOnly := bReadOnly;
  s := '';
  if IsSimpleJsonValue(p.JsonValue) then
  begin
    node := Items.AddChild(currNode, s + p.JsonString.value);
    Items.AddChild(node, s + p.JsonValue.ToString);
    exit;
  end;

  if p.JsonValue is TJSONObject then
  begin
    aCount := TJSONObject(p.JsonValue).Count;
    s := s + p.JsonString.Value;
    node := Items.AddChild(currNode, s);
    for i := 0 to aCount - 1 do
      ProcessPair(node, TJSONObject(p.JsonValue), i);
  end

  else
    if p.JsonValue is TJSONArray then
    begin
      s :=  p.JsonString.Value +'[]';
      aCount := TJSONArray(p.JsonValue).Count;
      node := Items.AddChild(currNode, s);
      for i := 0 to aCount - 1 do
        ProcessElement(node, TJSONArray(p.JsonValue), i);
    end
  //else
  // raise EUnknownJsonValueDescendant.Create;
end;


procedure TOpJSONTreeview.ProcessElement(currNode: TTreeNode; arr: TJSONArray; aIndex: integer);
var
  v: TJSONValue;
  s: string;
  n: TTreeNode;
  i, aCount: integer;
begin
  v := arr.Items[aIndex];
  s := ''; //'[' + IntToStr(aIndex) + '] ';

  if IsSimpleJsonValue(v) then   // Listenelement
  begin
    Items.AddChild(currNode, s + v.ToString);
    exit;
  end;

   if v is TJSONObject then
  begin
    aCount := TJSONObject(v).Count;
    s := s + ' {}';
    n := Items.AddChild(currNode, s);
    for i := 0 to aCount - 1 do
      ProcessPair(n, TJSONObject(v), i);
  end

  else if v is TJSONArray then
  begin
    aCount := TJSONArray(v).Count;
    s := s + ' []';
    n := Items.AddChild(currNode, s);
    for i := 0 to aCount - 1 do
      ProcessElement(n, TJSONArray(v), i);
  end
  //else
    //raise EUnknownJsonValueDescendant.Create;
end;


procedure TOpJSONTreeview.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;

  if Operation = opRemove then
    if FJSONObject <> nil then
      //if AComponent is FJSONObject then
      begin
        FJSONObject := nil;
        ClearAll;
      end;
end;

procedure TOpJSONTreeview.SetJSONObject(const Value: TJSONObject);
begin
   FJSONObject := Value;
   LoadJson;
end;

procedure TOpJSONTreeview.SetVisibleByteSizes(const Value: Boolean);
begin
  if FVisibleByteSizes <> Value then
  begin
    FVisibleByteSizes := Value;
    LoadJson;
  end;
end;

procedure TOpJSONTreeview.SetVisibleChildrenCounts(const Value: Boolean);
begin
  if FVisibleChildrenCounts <> Value then
  begin
    FVisibleChildrenCounts := Value;
    LoadJson;
  end;
end;

class function TOpJSONTreeview.UnQuote(s: string): string;
begin
  Result := s;
  if (Copy(Result,1,1) = '"') then
    Result := Copy(Result,2,Length(Result)-1);
  if (Copy(Result,Length(Result),1) = '"') then
    Result := Copy(Result,1,Length(Result)-1);
end;


end.
