unit main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls,
  {$IF CompilerVersion >= 28.0}System.Json,{$ENDIF}
  DBXJSON, uOpJSONTreeview, Vcl.Menus;

const
  GJSONString =
    '{' +
    '    "name": {'+
    '        "A JSON Object": {' +
    '          "id": "1"' +
    '        },' +
    '        "Another JSON Object": {' +
    '          "id": "2"' +
    '        }' +
    '    },' +
    '    "totalobjects": "2"' +
    '}';

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Memo1: TMemo;
    Button3: TButton;
    OpJSONTreeview1: TOpJSONTreeview;
    PopupMenu1: TPopupMenu;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private-Deklarationen }
    procedure DumpJason(aJsonObject : TJsonObject);
    function ParseTreeChild(currNode: TTreeNode; obj: TJsonObject) : boolean;
    function RemoveQuotation(aValue: String): String;
    function IsArray(Name: string): boolean;
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
   vJSONObject : TJSONObject;
   aGJSONString : string;
   jso: TJsonObject;
  jsv: TJsonValue;
  jsp: TJsonpair;
begin
  aGJSONString :=  '{"hardware": {"host": "192.168.2.71", "port": -1, "Test": { "IDLIST":[1,2,3,4,5]} },'+
                   ' "service": {"host": "0.0.0.0", "port": 5002},' +
                   ' "read_images_from_file": false, '+
                   ' "logging_mode": "always", '+
                   ' "Test": [1,2,3,4], ' +
                   ' "paths": '+
                     '{"mount": "C:\\DEVELOP\\Python\\Heliotis\\heli_api\\data", "log_dir": '+
                     '"C:\\DEVELOP\\Python\\Heliotis\\heli_api\\data\\log", '+
                     '"results": "C:\\DEVELOP\\Python\\Heliotis\\heli_api\\data\\results",'+
                     '"conf_file": "C:\\DEVELOP\\Python\\Heliotis\\heli_api\\data\\config.json",'+
                     '"halcon_lib": "C:\\DEVELOP\\Python\\Heliotis\\heli_api\\data\\halcon",'+
                     '"cam_params": "C:\\DEVELOP\\Python\\Heliotis\\heli_api\\data\\recipe",'+
                     '"hdev": "C:\\DEVELOP\\Python\\Heliotis\\heli_api\\data\\hdev", "recipe":'+
                     '"C:\\DEVELOP\\Python\\Heliotis\\heli_api\\data\\recipe"}}';
 // aGJSONString :=  '{"Test": { "IDLIST":[1,2,3,4,5]}}';
  vJSONObject := TJSONObject.Create;
  vJSONObject.Parse(BytesOf(aGJSONString),0);
  //JSONTreeView1.VisibleChildrenCounts := true;
  if Assigned(vJSONObject) then
    OpJSONTreeview1.JSONObject := vJSONObject;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
   aJason : TJSONObject;
begin
  //JSONTreeView1.JSONObject :=
  aJason := TJSONObject.Create;
  DumpJason(aJason);
end;

procedure TForm1.Button3Click(Sender: TObject);
var
   aJason : TJSONObject;
begin
  //JSONTreeView1.JSONObject :=
  aJason := TJSONObject.Create;
  OpJSONTreeview1.DumpJson(aJason);
  OpJSONTreeview1.JSONObject := aJason;
end;

function TForm1.RemoveQuotation(aValue : String) : String;
var
  trimmed : string;
begin
  trimmed := trim(aValue);
  if (pos('"',trimmed) = 1) and (copy(trimmed, length(trimmed),1) = '"') and (copy(trimmed, length(trimmed)-1,1) <> '\') then
  begin
    Result := copy(trimmed, 2 ,length(trimmed)-2);
  end else
    Result := aValue;
  Result := Result.Replace('\\','\');
end;

function TForm1.ParseTreeChild(currNode: TTreeNode; obj : TJsonObject) : boolean;
var
    loJSA  :  TJsonArray;
    loJSV  :  TJSONValue;
    loJSP  :  TJSONPair;
    loJSO  :  TJsonObject;
    child  :  TTreeNode;
    temp   : string;
    //listSuper : ISuperObject;  (SuperObject) .AsJSON
begin
   result := true;

   //loJSP := TJSONPair.Create('responseMessage', loJSA);
   //obj.AddPair(loJSP);
   //Memo1.Lines.Add(currNode.GetNextChild(currNode).Text);
   child := currNode.GetFirstChild;

   loJSA := TJsonArray.Create();
   loJSO := TJsonObject.Create();
   while Assigned(child) do
   begin
      if child.HasChildren then
      begin
        ParseTreeChild(child, loJSO);
        temp  :=  loJSO.Tostring;
      end else
      begin
        temp := child.Text;
        if pos('"',child.Text) > 0 then
          loJSA.Add(RemoveQuotation(child.Text))
        else if (child.Text = 'true') or (child.Text = 'false') then
          loJSA.Add(child.Text = 'true')
        else
          loJSA.Add(strtofloatdef(child.Text,0));
        temp := loJSA.toString
      end;
      child := currNode.GetNextChild(child);
   end;
   if currNode.GetFirstChild.HasChildren then
     loJSP := TJSONPair.Create(currNode.Text, loJSO)
   else begin
     temp := currNode.Text;
     if IsArray(temp) then
       temp := System.copy(temp,0,length(temp) - 2);
     if  (not IsArray(currNode.Text)) and (loJSA.Count = 1) then
       loJSP := TJSONPair.Create(temp, loJSA.Items[0] as TJSONvalue)
     else
       loJSP := TJSONPair.Create(temp, loJSA);
   end;
   //loJSA.Free;
   //loJSO.Free;
   obj.AddPair(loJSP);

end;

function TForm1.IsArray(Name : string) : boolean;
var
  temp : string;
begin
   Result := System.copy(Name, length(Name) - 1, 2) = '[]';
end;

procedure TForm1.DumpJason(aJsonObject : TJsonObject);
var
   TreeView :  TOpJSONTreeview;
   Node     :  TTreeNode;
   temp     : string;
   tem : tStringlist;
begin
  TreeView  := OpJSONTreeview1;
  if TreeView.Items.Count = 0 then
    Exit;
  Node := TreeView.Items[0].getFirstChild;
  Memo1.Lines.Add(inttostr(TreeView.Items.Count));
  if not Assigned(aJsonObject) then
    aJsonObject.Create;
  Memo1.Lines.Add('JSON:'+TJsonObject(aJsonObject).ToString);
  while Assigned(Node) do
  begin
     // Memo1.Lines.Add(Node.Text);  Den Ersten Node Umschiffen
     if Node.HasChildren then
       ParseTreeChild(Node, aJsonObject) ;
     Node := TreeView.Items[0].GetNextChild(Node);
  end;
   temp := TJsonObject(aJsonObject).ToString;
   Memo1.Lines.Add('JSON:'+temp);
   tem := tstringlist.create;
   tem.Add(temp);
   tem.SaveToFile('temp.txt');
   tem.Free;

  //aJsonObject.

end;

end.
