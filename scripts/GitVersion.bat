@echo off

set FILENAME=gitversion.inc

rem write informations in FILENAME in Delphi format!

rem set last update information
set LASTUPDATE=%date:~0,2%.%date:~3,2%.%date:~-4% %time:~0,2%:%time:~3,2%:%time:~6,2%
rem > override | >> append to file
echo const LASTUPDATE = '%LASTUPDATE%' > %FILENAME% ;

rem git last tag
@For /F "Delims=" %%i In ('"git describe --tags --abbrev=0"') Do @Set "GIT_LAST_TAG=%%i"
echo const GIT_LAST_TAG = '%GIT_LAST_TAG%' >> %FILENAME% ;

rem git short hashcode
@For /F "Delims=" %%i In ('"git log -n 1 --pretty=format:^"%%h^"') Do @Set "GIT_LAST_HASH=%%i"
echo const GIT_LAST_HASH = '%GIT_LAST_HASH%' >> %FILENAME% ;

rem git branch name
@For /F "Delims=" %%i In ('"git branch --show-current"') Do @Set "GIT_CURRENT_BRANCH=%%i"
echo const GIT_CURRENT_BRANCH = '%GIT_CURRENT_BRANCH%' >> %FILENAME% ;

rem git author name
@For /F "Delims=" %%i In ('"git log -n 1 --pretty=format:^"%%an^"') Do @Set "GIT_AUTHOR_NAME=%%i"
echo const GIT_AUTHOR_NAME = '%GIT_AUTHOR_NAME%' >> %FILENAME% ;

rem git commit date time
@For /F "Delims=" %%i In ('"git log -n 1 --pretty=format:^"%%cd^" --date=format:^"%%d.%%m.%%Y %%H:%%M:%%S^"') Do @Set "GIT_COMMIT_DATE=%%i"
echo const GIT_COMMIT_DATE = '%GIT_COMMIT_DATE%' >> %FILENAME% ;

@echo on